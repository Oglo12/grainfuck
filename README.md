# Grainfuck

Grainfuck is a simple Brainfuck interpreter written in Go.

It was written by Jackson Novak as a project to learn the Go programming language.

If you really want to do yourself a favor, program in Rust. :)
