// This is a minimal Brainfuck interpreter written in Go by Jackson Novak.
// It was written as a project to learn Go.
//
// Feel free to copy anything you see here. It is all licensed under the MIT license. :D
//
// (Seriously though, just use Rust.) https://rust-lang.org

package main

import (
	"fmt"
	"log"
	"os"
	"bufio"
)

type BrainfuckVM struct {
	tape [30000]uint8
	pointer int
	input_buffer []uint8
	scope int
}

func newVirtualMachine() BrainfuckVM {
	return BrainfuckVM {
		tape: [30000]uint8{},
		pointer: 0,
		input_buffer: []uint8{},
		scope: 0,
	}
}

func (self *BrainfuckVM) process(code string) {
	code_runes := []rune(code)
	var i int = 0
	for i < len(code_runes) {
		character := code_runes[i]

		switch character {
		case '+':
			self.tape[self.pointer] += 1
		case '-':
			self.tape[self.pointer] -= 1
		case '>':
			self.pointer += 1

			if self.pointer == 30000 {
				self.pointer = 0
			}
		case '<':
			self.pointer -= 1

			if self.pointer == -1 {
				self.pointer = 30000 - 1
			}
		case '.':
			fmt.Print(string(self.tape[self.pointer]))
		case ',':
			if len(self.input_buffer) == 0 {
				scanner := bufio.NewScanner(os.Stdin)
				scanner.Scan()
				if scanner.Err() != nil {
					errorOut(scanner.Err())
				}
				input_runes := []rune(scanner.Text())
				for j := 0; j < len(input_runes); j++ {
					self.input_buffer = append(self.input_buffer, uint8(input_runes[j]))
				}
				self.input_buffer = append(self.input_buffer, uint8('\n'))
			}

			if len(self.input_buffer) > 0 {
				self.tape[self.pointer] = self.input_buffer[0]
				self.input_buffer = self.input_buffer[1:]
			} else {
				self.tape[self.pointer] = 0
			}
		case '[':
			inner_code := []uint8{}
			offset := 1
			hits := 1
			// start := i + offset
			var end int
			for true {
				char := code_runes[i + offset]

				if char == '[' { hits++ }
				if char == ']' { hits-- }

				if hits == 0 {
					end = (i + offset) - 1

					break
				}

				inner_code = append(inner_code, uint8(char))

				offset++
			}
			inner_code_string := string(inner_code)
			self.scope++
			for self.tape[self.pointer] != 0 {
				self.process(inner_code_string)
			}
			self.scope--
			i = end + 1
		}

		i++
	}
}

func main() {
	args := os.Args[1:] // Get program arguments, and leave out the path to the binary.

	if len(args) != 2 {
		errorOut("Program needs 2 arguments. [ eval <SYNTAX> || file <FILE_PATH> ]")
	}

	var code string

	if args[0] == "eval" {
		code = args[1]
	} else if args[0] == "file" {
		contents, err := os.ReadFile(args[1])

		if err != nil {
			errorOut(err)
		}

		code = string(contents)
	} else {
		errorOut("Invalid argument! Please provide either: \"file\" or \"eval\"")
	}

	state := newVirtualMachine()
	state.process(code)
}

func errorOut(a ...any) {
	log.New(os.Stderr, "", 0).Println(a...)
	os.Exit(1)
}
